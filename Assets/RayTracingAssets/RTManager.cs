﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class RTManager : MonoBehaviour
{
    public static RTManager Instance;

    public Texture SkyboxTexture;
    public int NbRicochets;
    public Camera Camera;

    public ComputeShader RayTracingShaderWithoutTransparency;
    public ComputeShader RayTracingShaderWithTransparency;
    private ComputeBuffer ObjectsBuffer;
    private ComputeBuffer TrianglesBuffer;
    private ComputeBuffer LightsBuffer;

    private List<RTObject> rtObjects;
    private List<Triangle> triangles;
    private List<RTLight> lights;

    private bool sceneWithTransparency = false;

    struct Triangle
    {
        public Vector3 v0;
        public Vector3 v1;
        public Vector3 v2;
    }

    struct NNRayTracedObject   // Not Nullable Ray Traced Object
    {
        public Vector3 Albedo;
        public int IsTransparent;
        public float OpticalIndex;
        public float Reflectivity;
        public uint Offset;
        public uint Size;
    }

    struct NNLight  // Not Nullable Light
    {
        public int LightType;
        public Vector4 Coordinates;
        public Vector3 Color;
    }

    private void Awake()
    {
        Instance = this;
        rtObjects = new List<RTObject>();
        triangles = new List<Triangle>();
        lights = new List<RTLight>();

        if (Camera == null)
            Camera = GetComponent<Camera>();
    }

    private void OnApplicationQuit()
    {
        ObjectsBuffer?.Release();
        TrianglesBuffer?.Release();
    }

    public bool Register(RTObject rtObj)
    {
        foreach(RTObject obj in rtObjects)
            if (obj == rtObj)
                return false;

        var meshFilter = rtObj.GetComponent<MeshFilter>();
        if (meshFilter == null) return false;

        Mesh mesh = meshFilter.mesh;


        uint offset = (uint)triangles.Count;
        for (int i = 0; i < mesh.triangles.Length; i += 3)
            triangles.Add(new Triangle
            {
                v0 = rtObj.transform.TransformPoint(mesh.vertices[mesh.triangles[i + 0]]),
                v1 = rtObj.transform.TransformPoint(mesh.vertices[mesh.triangles[i + 1]]),
                v2 = rtObj.transform.TransformPoint(mesh.vertices[mesh.triangles[i + 2]])
            });

        rtObj.ID = NextRtObjectId();
        rtObj.Offset = offset;
        rtObj.Size = (uint) triangles.Count - offset;


        rtObjects.Add(rtObj);
        ReloadBuffers();

        return true;
    }

    public bool Register(RTLight light)
    {
        foreach(RTLight l in lights)
            if (l == light)
                return false;

        light.ID = NextRtLightId();

        lights.Add(light);
        ReloadLightsBuffer();

        return true;
    }

    public bool RemoveObject(RTObject rtObj)
    {
        if (rtObj == null) return false;
        if (rtObj.mesh == null) return false;
        int i = rtObjects.FindIndex(0, rtObjects.Count, (obj) => rtObj.ID == obj.ID);
        if (i < 0) return false;

        uint end = rtObj.Offset + rtObj.Size;
        for(int j = 0; j < rtObjects.Count; j++)
            if (rtObjects[j].Offset >= end)
                rtObjects[j].Offset -= rtObj.Size;


        triangles.RemoveRange((int)rtObj.Offset, (int)rtObj.Size);
        rtObjects.RemoveAt(i);

        ReloadBuffers();

        return true;
    }

    public bool RemoveLight(RTLight light)
    {
        if (light == null) return false;
        int i = lights.FindIndex(0, lights.Count, (l) => light.ID == l.ID);
        if (i < 0) return false;

        lights.RemoveAt(i);
        ReloadLightsBuffer();

        return true;
    }

    public bool UpdateObject(RTObject rtObj)
    {
        if (rtObj == null) return false;
        if (rtObj.mesh == null) return false;
        int i = rtObjects.FindIndex(0, rtObjects.Count, (obj) => rtObj.ID == obj.ID);
        if (i < 0) return false;

        rtObjects[i].Albedo = rtObj.Albedo;
        ReloadObjectBuffer();

        return true;
    }

    public bool UpdateTriangles(RTObject rtObj)
    {
        if (rtObj.mesh == null) return false;
        int i = rtObjects.FindIndex(0, rtObjects.Count, (obj) => rtObj.ID == obj.ID);
        if (i < 0) return false;

        if (rtObj.Size != rtObj.mesh.triangles.Length / 3)
            return false;

        uint index = rtObj.Offset;
        for (int j = 0; j < rtObj.mesh.triangles.Length; j+=3, index++)
        {
            triangles[(int)index] = new Triangle
            {
                v0 = rtObj.transform.TransformPoint(rtObj.mesh.vertices[rtObj.mesh.triangles[j + 0]]),
                v1 = rtObj.transform.TransformPoint(rtObj.mesh.vertices[rtObj.mesh.triangles[j + 1]]),
                v2 = rtObj.transform.TransformPoint(rtObj.mesh.vertices[rtObj.mesh.triangles[j + 2]])
            };
        }

        ReloadTriangleBuffer();

        return true;
    }

    public bool UpdateLight(RTLight light)
    {
        if (light == null) return false;

        int i = lights.FindIndex(0, lights.Count, (l) => l.ID == light.ID);
        if (i < 0) return false;

        lights[i] = light;
        ReloadLightsBuffer();

        return true;
    }

    private uint NextRtObjectId()
    {
        uint id = 0;
        bool idUsed;
        do
        {
            idUsed = false;
            foreach (RTObject rtObj in rtObjects)
                if (id == rtObj.ID)
                {
                    idUsed = true;
                    id++;
                    break;
                }
        } while (idUsed);
        return id;
    }

    private uint NextRtLightId()
    {
        uint id = 0;
        bool idUsed;
        do
        {
            idUsed = false;
            foreach (RTLight light in lights)
                if (id == light.ID)
                {
                    idUsed = true;
                    id++;
                    break;
                }
        } while (idUsed);
        return id;
    }

    private void ReloadBuffers()
    {
        ReloadObjectBuffer();
        ReloadTriangleBuffer();
    }

    private void ReloadObjectBuffer()
    {
        RTCamera.MainCamera.ResetAntiAliasing();
        ObjectsBuffer?.Release();

        const int stride = 4 + 4 + 4 * 3 + 4 + 4 + 4;

        if(rtObjects.Count <= 0)
        {
            Debug.LogWarning("No object in buffer");
            return;
        }

        List<NNRayTracedObject> buffer = new List<NNRayTracedObject>();
        sceneWithTransparency = false;
        foreach (RTObject obj in rtObjects)
        {
            buffer.Add(new NNRayTracedObject
            {
                Albedo = obj.Albedo,
                IsTransparent = (obj.IsTransparent) ? 1 : 0,
                OpticalIndex = obj.OpticalIndex,
                Reflectivity = obj.Reflectivity,
                Offset = obj.Offset,
                Size = obj.Size
            });
            if (obj.IsTransparent)
                sceneWithTransparency = true;
        }

        ObjectsBuffer = new ComputeBuffer(buffer.Count, stride);
        ObjectsBuffer.SetData(buffer);
    }

    private void ReloadTriangleBuffer()
    {
        RTCamera.MainCamera.ResetAntiAliasing();
        TrianglesBuffer?.Release();

        const int stride = 4 * 3 * 3;
        if (triangles.Count <= 0)
        {
            Debug.LogWarning("No triangles in buffer");
            return;
        }
        TrianglesBuffer = new ComputeBuffer(triangles.Count, stride);
        TrianglesBuffer.SetData(triangles);
    }

    private void ReloadLightsBuffer()
    {
        RTCamera.MainCamera.ResetAntiAliasing();
        LightsBuffer?.Release();

        const int stride = 4 + 4 * 4 + 4 * 3;

        if (lights.Count <= 0)
        {
            Debug.LogWarning("No light in buffer");
            return;
        }

        List<NNLight> tmpLights = new List<NNLight>();
        foreach (RTLight l in lights)
        {
            NNLight tmp = new NNLight
            {
                Color = new Vector3(l.Color.r, l.Color.g, l.Color.b),
            };

            switch (l.LightType)
            {
                case RTLightType.Directional: tmp.LightType = 0;
                    break;
                case RTLightType.Point: tmp.LightType = 1;
                    break;
                case RTLightType.Led: tmp.LightType = 2;
                    break;
                default: tmp.LightType = 0;
                    break;
            }

            if (tmp.LightType == 1 || tmp.LightType == 2)
            {
                Vector3 pos = l.transform.position;
                tmp.Coordinates = new Vector4(pos.x, pos.y, pos.z, l.Intensity);
            }
            else
            {
                tmp.Coordinates = l.transform.TransformDirection(l.transform.forward);
                tmp.Coordinates.w = l.Intensity;
            }

            tmpLights.Add(tmp);
        }

        LightsBuffer = new ComputeBuffer(lights.Count, stride);
        LightsBuffer.SetData(tmpLights);
    }

    public bool SetShaderParams(ComputeShader shader)
    {
        if (Camera == null || SkyboxTexture == null || LightsBuffer == null || ObjectsBuffer == null ||
            TrianglesBuffer == null)
            return false;
        shader.SetMatrix("_CameraToWorld", Camera.cameraToWorldMatrix);
        shader.SetMatrix("_CameraInverseProjection", Camera.projectionMatrix.inverse);
        shader.SetTexture(0, "_SkyboxTexture", SkyboxTexture);
        shader.SetBuffer(0, "_Lights", LightsBuffer);
        shader.SetFloat("_NbRicochets", NbRicochets);
        shader.SetBuffer(0, "_Objects", ObjectsBuffer);
        shader.SetBuffer(0, "_Triangles", TrianglesBuffer);
        return true;
    }

    public void Render(RenderTexture destination, Vector2 pixelOffset)
    {
        if (destination == null) return;

        int threadGroupsX = Mathf.CeilToInt(Screen.width / 8.0f);
        int threadGroupsY = Mathf.CeilToInt(Screen.height / 8.0f);

        ComputeShader shader = (sceneWithTransparency)
            ? RayTracingShaderWithTransparency
            : RayTracingShaderWithoutTransparency;

        if (!SetShaderParams(shader))
        {
            Debug.LogWarning("Some shader parameters are not defined");
            return;
        }

        shader.SetVector("_PixelOffset", pixelOffset);
        shader.SetTexture(0, "Result", destination);
        shader.Dispatch(0, threadGroupsX, threadGroupsY, 1);
    }
}
