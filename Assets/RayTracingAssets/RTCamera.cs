﻿using System.Collections.Generic;
using System.Net.WebSockets;
using UnityEditor.Rendering;
using UnityEngine;

public class RTCamera : MonoBehaviour
{
    public static RTCamera MainCamera;

    public Vector2Int Resolution;
    public bool EnableAntiAliasing;
    public bool FPSCounter;

    private RenderTexture renderTexture;
    private new Camera camera;

    private uint currentSample = 0;
    private Material aaMaterial;

    private float avgFPS;
    private int sampleNumber;

    public void Awake()
    {
        MainCamera = this;
        camera = GetComponent<Camera>();
        currentSample = 0;
        avgFPS = 0;
        sampleNumber = 0;
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Backspace))
            FPSCounter = true;
        if (Input.GetKeyUp(KeyCode.Backspace))
            FPSCounter = false;

        if (transform.hasChanged)
        {
            ResetAntiAliasing();
            transform.hasChanged = false;
        }
    }

    public void ResetAntiAliasing()
    {
        currentSample = 0;
    }

    public void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Render(destination);
    }

    public void Render(RenderTexture destination)
    {
        // Create render target
        // If the target is not initialized yet
        var resolution = Resolution;
        if (Resolution.x == 0 || Resolution.y == 0)
            resolution = new Vector2Int(Screen.width, Screen.height);

        if (renderTexture == null || renderTexture.width != resolution.x || renderTexture.height != resolution.y)
        {
            if (renderTexture != null) renderTexture.Release();
            renderTexture = new RenderTexture(resolution.x, resolution.y, 0, RenderTextureFormat.ARGBFloat, RenderTextureReadWrite.Linear)
            {
                enableRandomWrite = true
            };
            renderTexture.Create();
            Debug.Log($"Screen size: {Screen.width}x{Screen.height} ");
        }

        Vector2 offset = Vector2.zero;
        if (EnableAntiAliasing)
        {
            if (currentSample != 0)
                offset = new Vector2(Random.value, Random.value);
        }
        else
            ResetAntiAliasing();

        RTManager.Instance.Render(renderTexture, offset);
        if (FPSCounter)
        {
            if (sampleNumber > 0)
                avgFPS *= sampleNumber;
            avgFPS += 1 / Time.deltaTime;
            avgFPS /= ++sampleNumber;
            Debug.Log($"AVG FPS: {avgFPS}");
        }
        else
        {
            avgFPS = 0;
            sampleNumber = 0;
        }

        // Anti alliasing
        if (EnableAntiAliasing)
        {
            if (aaMaterial == null)
            {
                currentSample = 0;
                aaMaterial = new Material(Shader.Find("Hidden/AAShader"));
            }

            aaMaterial?.SetFloat("_Sample", currentSample++);

            Graphics.Blit(renderTexture, destination, aaMaterial);
        }
        else
            Graphics.Blit(renderTexture, destination);
    }
}
