﻿using System;
using UnityEngine;
using UnityEngine.UIElements;

public class RTObject : MonoBehaviour
{
    public uint ID { get => id; set => id = value; }
    private uint id;

    public Vector3 Albedo;
    public bool IsTransparent;
    public float OpticalIndex;
    [Range(0, 1)]
    public float Reflectivity;

    public uint Offset { get => offset; set => offset = value; }
    private uint offset;

    public uint Size { get => size; set => size = value; }
    private uint size;

    public Mesh mesh { get; private set; }

    private bool needReload;

    public void Start()
    {
        var meshFilter = GetComponent<MeshFilter>();
        if (meshFilter == null)
            return;
        mesh = meshFilter.mesh;
        needReload = false;

        RTManager.Instance.Register(this);
    }

    public void Update()
    {
        if (needReload)
        {
            RTManager.Instance.UpdateObject(this);
            needReload = false;
        }

        if (transform.hasChanged)
        {
            RTManager.Instance.UpdateTriangles(this);
            transform.hasChanged = false;
        }
    }

    public void OnDisable()
    {
        RTManager.Instance.RemoveObject(this);
    }

    public void SetAlbedo(Vector3 albedo)
    {
        Albedo = albedo;
        needReload = true;
    }

    public void SetReflectivity(float reflectivity)
    {
        Reflectivity = reflectivity;
        needReload = true;
    }

    public void SetOpticalIndex(float opticalIndex)
    {
        OpticalIndex = opticalIndex;
        needReload = true;
    }

    public void SetTransparency(bool isTransparent)
    {
        IsTransparent = isTransparent;
        needReload = true;
    }

    public void OnValidate()
    {
        needReload = true;
    }
}
