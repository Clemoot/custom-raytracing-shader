﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RTLightType
{
    Directional,
    Point,
    Led
}

public class RTLight : MonoBehaviour
{

    public uint ID { get; set; }

    public RTLightType LightType;
    [Range(0, 50)]
    public float Intensity;
    public Color Color;

    private bool needReload;

    // Start is called before the first frame update
    void Start()
    {
        needReload = false;
        RTManager.Instance.Register(this);
    }

    void Update()
    {
        if (needReload || transform.hasChanged)
        {
            RTManager.Instance.UpdateLight(this);
            needReload = false;
            transform.hasChanged = false;
        }
    }

    void OnValidate()
    {
        needReload = true;
    }

    void OnDisable()
    {
        RTManager.Instance.RemoveLight(this);
    }
}
