﻿using System.Collections.Generic;
using UnityEngine;

public class Duplicate : MonoBehaviour
{
    public GameObject DuplicatedGameObject;
    [Range(0, 100)] public int Number;
    [Range(5, 250)] public float Distance;

    private static List<GameObject> duplicates;

    private bool reloadDuplicates = false;

    void Update()
    {
        if(reloadDuplicates)
        {
            if(duplicates != null)
                for(int i = 0; i < duplicates.Count; i++)
                    Destroy(duplicates[i]);
            duplicates = new List<GameObject>();
            GenerateDuplicates();
            reloadDuplicates = false;
        }
    }

    void OnValidate()
    {
        reloadDuplicates = true;
    }

    void OnDestroy()
    {
        if (duplicates != null)
        {
            foreach(GameObject go in duplicates)
                if(go != null)
                    Destroy(go);
            duplicates.Clear();
            duplicates = null;
        }
    }

    void GenerateDuplicates()
    {
        if (duplicates == null) return;
        if (duplicates.Count > 0) return;
        for (int i = 0; i < Number; i++)
        {
            GameObject go = Instantiate(DuplicatedGameObject);
            go.SetActive(true);
            RTObject rt = go.GetComponent<RTObject>();
            rt.SetAlbedo(new Vector3(Random.value, Random.value, Random.value));
            rt.SetReflectivity(Random.value);

            go.transform.position = transform.position + Distance * new Vector3(Random.value * 2  - 1, 0, Random.value * 2 - 1);
            go.transform.parent = transform;
            duplicates.Add(go);
        }
    }
}
