﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleTransparency : MonoBehaviour
{
    private RTObject obj;
    private Coroutine routine;

    void Start()
    {
        obj = GetComponent<RTObject>();
        if (obj != null)
            routine = StartCoroutine("Toggle");
    }

    void OnEnable()
    {
        if (routine != null && obj != null)
            routine = StartCoroutine("Toggle");
    }

    void OnDisable()
    {
        if(routine != null)
            StopCoroutine(routine);
    }

    IEnumerator Toggle()
    {
        while (true)
        {
            yield return new WaitForSeconds(2);
            obj.SetTransparency(!obj.IsTransparent);
        }
    }
}
