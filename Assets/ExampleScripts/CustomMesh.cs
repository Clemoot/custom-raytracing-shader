﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MeshType
{
    Ellipsoid,
    Cylinder
}

public class CustomMesh : MonoBehaviour
{
    public MeshType MeshType;

    // Start is called before the first frame update
    void Awake()
    {
        if(MeshBuilder.instance == null)
            MeshBuilder.instance = new MeshBuilder();

        Mesh mesh = GetComponent<MeshFilter>().mesh;
        Vector3[] vertices;
        int[] triangles;
        Vector2[] uvs;
        MeshBuilder.instance.CreateEllipsoid(20, 40, Matrix4x4.identity, out vertices, out triangles, out uvs, 10, 10);
        mesh.SetVertices(vertices);
        mesh.SetTriangles(triangles, 0);
        mesh.SetUVs(0, uvs);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
