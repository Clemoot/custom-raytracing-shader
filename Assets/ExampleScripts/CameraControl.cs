﻿using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public float speed;
    public float sensivity;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Return))
            this.enabled = false;
        if (Input.GetKey(KeyCode.Space))
            Cursor.lockState = CursorLockMode.None;

        float verticalInput = .0f;
        if (Input.GetKey(KeyCode.E))
            verticalInput += 1;
        if (Input.GetKey(KeyCode.A))
            verticalInput -= 1;

        Vector3 mouseWorld = Input.GetAxis("Mouse X") * Vector3.up * sensivity * Time.deltaTime;
        Vector3 mouseLocal = -Input.GetAxis("Mouse Y") * Vector3.right * sensivity * Time.deltaTime;
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles + mouseWorld);
        transform.localRotation = Quaternion.Euler(transform.localRotation.eulerAngles + mouseLocal);

        Vector3 movement = new Vector3(Input.GetAxis("Horizontal"), verticalInput, Input.GetAxis("Vertical")) * speed * Time.deltaTime;
        movement = transform.TransformDirection(movement);
        transform.position += movement;
    }
}
