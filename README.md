# Custom Raytracing Shader

This is a custom raytracing shader without any hardware acceleration. It is a compute shader. It is implemented in Unity.
Thee shader is written in HLSL and is iterative. Consequently, it works with the lightweight render pipeline
It implements basic lighting physics : refraction and reflection. It supports multiple lighting, but costs a lot and render shadows.

## Files

**RTCamera.cs :** This script must be attached to the Camera. It calls RTRegister::Render to each frame to generate the image.

**RTManager.cs :** It is a singleton. It must be attached to any object (usually the Camera) in the scene. This script registers every object and light, which should be rendered. It reloads buffers for the compute shader. **It is basically the interface between CPU and GPU**.

**RTObject.cs :** This script describes an object in the scene and automatically registers it in RTManager. The variable `needReload` allows to reload object data. Vertices are updated when the transform is changed.

**RTLight.cs :** This script describes the basic information of a light and automatically registers it. It is updated with the variable `needReload`.

**SimpleRTShader.compute** and **CompleteRTShader.compute :** These are the both shaders to compute the image.

Since the algorithm should be recursive, we have to make it iterative. This is the main difference between the both shaders.
On one hand, the simple one cannot create new rays from another one. On the other hand, the complete shader can split a ray into multiple rays and compute greater features like refraction.
However, this feature costs a lot of performance - at least 20 FPS -. That is why the *RTManager* script automatically changes the shader when it detects a transparent object.

## Shader parameters

The shader is 4 main parameters :
- `_NbRicochets`: It defines how many times a ray can bounce
- `_Objects`: It is an array containing all the rendered object data. The data passed is defined by the structure *RayTracedObject* :
```c
struct RayTracedObject {
    float3 albedo;          // Color of the object
    int isTransparent;      // Is the object transparent ?
    float opticalIndex;     // If the object is transparent, it is its optical index
    float reflectivity;     // If not transparent, it is the reflectivity of the surface, otherwise, it is determined according to optical index
    uint offset;            // Triangles array offset
    uint size;              // Triangles array size
};
```
- `_Triangles`: It is the array containing every triangles (and vertices) of each object. You can retrieve the triangles of an object by using the `offset` and `size` fields of the structure `RayTracedObject`.
```c
struct Triangle {
    float3 v0;
    float3 v1;
    float3 v2;
};
```
- `_Lights`: it is the array containing every light.
```c
struct Light {
    int type;       // Type of the light
    float4 coord;   // Light coordinates
    float3 color;   // Color of the light
}
```
On C# side, the `RTLightType` enumeration defines the type of light :
```c
enum RTLightType {
    Directional,
    Point,
    Led
}
```
The light type changes the coordinates passed to the shader. 
For `Directional` light, the coordinates passed are the direction of the light. 
For `Point` and `Led` light, the coordinates are the position of the light.
The difference between `Point`and `Led` lights is the attenuation function :
- `Point`: `attenuation = 1 / distance`
- `Led`: `attenuation = exp(-distance)`

## Examples

Two Unity scenes are provided as examples.

### Demo

This example shows the features and performance of the shader. You can change the number of triangles in the scene and switch between the both shaders.
You can also see the refraction feature and try multiple lights.

### InfiniteMirror

This scene may not be executed on most of computers. This example is an infinite mirror (the one you can see in DIY videos). It uses all the features of the shader (refraction/reflection, multiple lights, shadows).
You can change the number of ricochets to make the effect more realistic.

*Since the anti-aliasing algorithm provided is progressive anti-aliasing, you may need to disable/enable it to reset the main frame*